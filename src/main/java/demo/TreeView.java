package demo;

import demo.model.Comment;
import demo.model.Item;
import demo.model.ItemModel;
import org.apache.commons.lang3.time.DateUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.*;

import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

public class TreeView {
    private ItemModel model;
    List<Comment> comments;
    ListModelList<Item> itemHasC = new ListModelList<>();

    @Init(superclass = true)
    public void init() throws ParseException {
        List<Item> raw = new ArrayList<>(Arrays.asList(
                new Item(1, "Item 1", -1),
                new Item(2, "Item 2", 1),
                new Item(3, "Item 3", 1),
                new Item(4, "Item 4", 2, false),
                new Item(5, "Item 5", 3),
                new Item(6, "Item 6", 3),
                new Item(7, "Item 7", 6),
                new Item(8, "Item 8", 6, false),
                new Item(9, "Item 9", 1, false),
                new Item(10, "Item 10", 7),
                new Item(11, "Item 11", 5, false),
                new Item(12, "Item 12", 10, false)
        ));

        comments = new ArrayList<>(Arrays.asList(
                new Comment(1, "Comment 1", DateUtils.parseDate("11/5/2019", "dd/MM/yyyy"), 3),
                new Comment(2, "Comment 2", DateUtils.parseDate("12/5/2019", "dd/MM/yyyy"), 3),
                new Comment(3, "Comment 3", DateUtils.parseDate("9/10/2019", "dd/MM/yyyy"), 8),
                new Comment(4, "Comment 4", DateUtils.parseDate("12/5/2019", "dd/MM/yyyy"), 11),
                new Comment(5, "Comment 5", DateUtils.parseDate("25/12/2019", "dd/MM/yyyy"), 12)
        )
        );

        model = new ItemModel(new Item(-1, "", null), raw);
        itemHasC.addAll(getItemG());
    }

    @NotifyChange("model")
    @Command
    public void edit(@BindingParam("item") Item item) {
        item.setEdit(true);
    }

    @NotifyChange("model")
    @Command
    public void closeEdit(@BindingParam("item") Item item) {
        item.setEdit(false);
    }

    @NotifyChange("model")
    @Command
    public void delete(@BindingParam("item") Item item) {
        model.getRawList().remove(item);
    }

    @NotifyChange("model")
    @Command
    public void newItem(@BindingParam("parent") Item parent) {
        Integer currentId = model.getRawList().stream().map(Item::getId).max(Integer::compareTo).orElse(0);
        Item newItem = new Item(currentId + 1, "New Item", parent.getId(), false);
        newItem.setEdit(true);
        parent.setOpen(true);
        model.getRawList().add(newItem);
    }

    @Command
    public void moreRequest() {
        Messagebox.show("Drag and drop to see what happen!");
    }

    @NotifyChange("model")
    @Command
    public void dragAndDrop(@BindingParam("drag") Item drag, @BindingParam("drop") Item drop) {
        drag.setParentId(drop.getId());
        model.openAll();
    }

    @NotifyChange("model")
    @Command
    public void addComment(@BindingParam("comment") String comment,
                           @BindingParam("item") Item item) {
        Integer currentId = comments.stream().map(Comment::getId).max(Integer::compareTo).orElse(0);
        Comment newComment = new Comment(currentId + 1, comment, new Date(), item.getId());
        comments.add(newComment);
        model.getRawList().forEach(r -> r.setPopup(false));
        item.setPopup(true);
        itemHasC.clear();
        itemHasC.addAll(getItemG());
    }

    @NotifyChange("model")
    @Command
    public void openComment(@BindingParam("item") Item item) {
        model.getRawList().forEach(r -> r.setPopup(false));
        item.setPopup(true);
    }

    public List<Comment> getComments(Integer itemId) {
        return comments.stream().filter(c -> itemId.equals(c.getItemId())).collect(Collectors.toList());
    }

    public long getCommentsCount(Item item) {
        return comments.stream().filter(c -> item.getId().equals(c.getItemId())).count();
    }

    private List<Item> getItemG(){
        List<Integer> listIds = comments.stream().map(Comment::getItemId).collect(Collectors.toList());
        return model.getRawList().stream().filter(m->listIds.contains(m.getId())).collect(Collectors.toList());
    }

    public ItemModel getModel() {
        return model;
    }

    public ListModelList<Item> getItemHasC() {
        return itemHasC;
    }
}
