package demo.model;

import org.zkoss.zul.AbstractTreeModel;

import java.util.List;
import java.util.stream.Collectors;

public class ItemModel extends AbstractTreeModel<Item> {
    private List<Item> rawList;

    public ItemModel(Item root, List<Item> rawList) {
        super(root);
        this.rawList = rawList;
    }

    @Override
    public boolean isLeaf(Item item) {
        return getChildCount(item) == 0;
    }

    @Override
    public Item getChild(Item item, int i) {
        if (rawList == null || rawList.isEmpty() || getChildCount(item) <= i) {
            return null;
        } else {
            return rawList.stream().filter(f -> item.getId().equals(f.getParentId())).collect(Collectors.toList()).get(i);
        }
    }

    @Override
    public int getChildCount(Item item) {
        if (rawList == null || rawList.isEmpty()) {
            return 0;
        } else {
            return (int) rawList.stream().filter(f -> item.getId().equals(f.getParentId())).count();
        }
    }

    public void openAll() {
        rawList.forEach(i -> i.setOpen(!isLeaf(i)));
    }

    public List<Item> getRawList() {
        return rawList;
    }

    public void setRawList(List<Item> rawList) {
        this.rawList = rawList;
    }
}
