package demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Item {
    private Integer id;
    private String name;
    private Integer parentId;
    private boolean open;
    private boolean edit;
    private boolean popup;

    public Item(Integer id, String name, Integer parentId) {
        this.id = id;
        this.name = name;
        this.parentId = parentId;
        this.open = true;
    }

    public Item(Integer id, String name, Integer parentId, boolean open) {
        this.id = id;
        this.name = name;
        this.parentId = parentId;
        this.open = open;
    }
}
